Source: workrave
Section: gnome
Priority: optional
Maintainer: Francois Marier <francois@debian.org>
Uploaders:
 Jordi Mallach <jordi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 autoconf-archive,
 docbook-utils,
 gobject-introspection,
 gsettings-desktop-schemas-dev,
 intltool,
 libayatana-indicator3-dev,
 libboost-dev,
 libdbusmenu-glib-dev,
 libdbusmenu-gtk3-dev,
 libgdome2-dev,
 libgirepository1.0-dev,
 libglib2.0-0t64,
 libglibmm-2.4-dev,
 libgnome-panel-dev (>= 3.37.1),
 libgstreamer1.0-dev,
 libgtk-3-dev,
 libgtk-4-dev,
 libgtkmm-3.0-dev,
 libmate-panel-applet-dev,
 libpulse-dev,
 libsigc++-2.0-dev,
 libxfce4panel-2.0-dev,
 libxfce4ui-2-dev,
 libxi-dev,
 libxmu-dev,
 libxss-dev,
 libxtst-dev,
 python3-jinja2,
 xmlto,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://workrave.org/
Vcs-Git: https://salsa.debian.org/debian/workrave.git
Vcs-Browser: https://salsa.debian.org/debian/workrave

Package: workrave-ayatana
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 workrave (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Repetitive Strain Injury prevention tool (Ayatana Indicator)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package includes a Workrave Ayatana Indicator for desktop
 environments that are capable of displaying them.

Package: workrave-cinnamon
Architecture: all
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 cinnamon-core,
 workrave (>= ${source:Version}),
 ${misc:Depends},
Description: Repetitive Strain Injury prevention tool (Cinnamon integration)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package includes a Workrave extension for Cinnamon.

Package: workrave-gnome
Architecture: all
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 gnome-shell (<< 49~),
 gnome-shell (>= 45~),
 workrave (>= ${source:Version}),
 ${misc:Depends},
Description: Repetitive Strain Injury prevention tool (GNOME integration)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package includes a Workrave GNOME Shell extension.

Package: workrave-gnome-flashback
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 gnome-panel,
 workrave (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Repetitive Strain Injury prevention tool (GNOME panel applet)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package includes a Workrave applet for GNOME Flashback.

Package: workrave-mate
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 mate-panel,
 workrave (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Repetitive Strain Injury prevention tool (MATE panel applet)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package includes a Workrave applet for the MATE panel.

Package: workrave-xfce4
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 workrave (>= ${binary:Version}),
 xfce4-panel,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Repetitive Strain Injury prevention tool (Xfce4 panel plugin)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package includes a Workrave plugin for the Xfce4 panel.

Package: workrave
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 workrave-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 workrave-data (<< 1.10.51.1-4),
Suggests:
 workrave-ayatana,
 workrave-cinnamon,
 workrave-gnome,
 workrave-gnome-flashback,
 workrave-mate,
 workrave-xfce4,
Description: Repetitive Strain Injury prevention tool
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.

Package: workrave-data
Architecture: all
Depends:
 ${misc:Depends},
Description: Repetitive Strain Injury prevention tool (data files)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package contains the required data files common to all architectures.
